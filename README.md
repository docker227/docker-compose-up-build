# `docker compose up --build` doesn't use new image

When making changes to something that affects an image that a service uses, running `docker compose up --build` should rebuild that image and then recreate the corresponding service's container so that it uses the new image.

> If there are existing containers for a service, and the service’s configuration or image was changed after the container’s creation, docker-compose up picks up the changes by stopping and recreating the containers (preserving mounted volumes). To prevent Compose from picking up changes, use the --no-recreate flag.
See https://docs.docker.com/compose/reference/up/.

I've noticed recently that this is not the case, although this has worked well for me in the past.

This appears to have been a problem in the past:
- https://github.com/docker/compose-cli/issues/1608
- https://github.com/docker/compose-cli/pull/1530

## Versions

ф docker --version
Docker version 20.10.12, build e91ed5707e
ф docker compose version
Docker Compose version 2.3.3

## Steps to recreate the issue
- `docker compose up --build`
- `docker compose images`
    - note the `Image Id`
- modify `main.py`
- `docker compose up --build`
    - a new image will be created because a depdency (`main.py`) has changed
- `docker compose images`
    - the old `Image Id` is still listed
- `docker compose up --build --force-recreate`
- `docker compose images`
    - the new `Image Id` is now listed
